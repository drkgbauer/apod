#!/bin/bash
cd ~/projects/apod
python3 apod/apod.py
cp public/*.json /var/www/html
cp public/images/* /var/www/html/images
git commit -m "current imagelist.json" public/imagelist.json
git push origin master
