#!/bin/sh
sudo pip3 install --upgrade pip
sudo pip3 install -r requirements.txt 
sudo mkdir -p /data/logs
sudo chown pi /data/logs
sudo chmod -R 777 /data
python apod/apod.py --install
crontab crontab.apod
