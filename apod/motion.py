import RPi.GPIO as GPIO
import time

pir_sensor = 13
GPIO.setmode(GPIO.BOARD)
GPIO.setup(pir_sensor, GPIO.IN)
current_state = 0

def go():
    try:
        while True:
            time.sleep(0.1)
            current_state = GPIO.input(pir_sensor)
            if current_state == 1:
                print("GPIO pin %s is %s" % (pir_sensor, current_state))
                # GPIO.output(piezo,True)
                # time.sleep(1)
                # GPIO.output(piezo,False)
            else:
                print("GPIO pin %s is %s" % (pir_sensor, current_state))
            time.sleep(5)
    except KeyboardInterrupt:
        pass
    finally:
        GPIO.cleanup()
    
if __name__ == "__main__":
    go()