#!/user/bin/env python3
"""
APOD is a utility to generate a JSON list of NASA's astronomy picture of the
day images and explanations.

/usr/bin/google-chrome --start-fullscreen

"""
import urllib.request
import urllib.error
import urllib.parse
#import urllib2
import os
import json
# import pprint
from datetime import date
#from datetime import timedelta
import easylog

BASE_URL = "https://apod.nasa.gov/apod"

log = easylog.setup_logging('apod', cfg='logging.yml', subdir=True)


def read_url(url):
    """ return the html text of the page """
    response = urllib.request.urlopen(url)
    stuff = response.read()
    stuff = stuff.decode('utf-8')
    return stuff

#    response = urllib2.urlopen(url)
#    return response.read()


def get_image_list(num=60):
    """
    Hit NASA's api to get a JSON list of images.
    """
    api_key = "ALdYySB7BN4ii6bT6rUCKSkS3y9vRCzpj0vVP40J"
    url = "https://api.nasa.gov/planetary/apod?api_key=%s&count=%d" % (
        api_key, num)
    log.info("url: %s", url)
    imagestr = read_url(url)
    imagelist = json.loads(imagestr)
    videos = [x for x in imagelist if x['media_type'] == 'video']
    for vvv in videos:
        vvv['url'] += '&autoplay=1'
    log.info("add autoplay to %d videos", len(videos))
    return imagelist


def get_image_info(thedate=None):
    """
    Given a
    """
    if thedate is None:
        thedate = date.today()
    query = {'api_key': "ALdYySB7BN4ii6bT6rUCKSkS3y9vRCzpj0vVP40J",
             'date': thedate.strftime('%Y-%m-%d')}
    url = "https://api.nasa.gov/planetary/apod?%s" % urllib.parse.urlencode(
        query)
    log.info("url: %s", url)
    image_info = read_url(url)
    return json.loads(image_info)


def main():
    """ main function to run"""
    #imagelist = build_image_list(N)
    for iii in range(10):
        try:
            imagelist = get_image_list(N)
        except:
            log.exception("error getting image list")
        else:
            break
        log.info("try %d failed", iii)

    output = os.path.join(os.path.dirname(__file__), "..", "public",
                          "imagelist.json")
    log.info("output: %s", output)
    fd_out = open(output, 'w')
    json.dump(imagelist, fd_out)


def update_image_list(thedate=None):
    """
    Get the image info for today and add it to the image list if it
    isn't their.
    """
    output_dir = os.path.join(os.path.dirname(
        __file__), "..", "public", "images")
    master_file = os.path.join(os.path.dirname(
        __file__), "..", "public", "imagelist.json")
    master_list = json.load(open(master_file))
    log.info("master list has %d items", len(master_list))
    info = get_image_info(thedate)
    info['local_url'] = os.path.join("images", os.path.basename(info['url']))
    if info not in master_list:
        master_list.append(info)
        get_img(info['url'], output_dir)
        log.info("adding info to master_list")
        json.dump(master_list, open(master_file, 'w'), indent=2)
    else:
        log.info("current image %s is already in list", info['local_url'])


def pull_all_images():
    """
    make sure we have all the images in the image list and the
    local_url is set.
    """
    output_dir = os.path.join(os.path.dirname(
        __file__), "..", "public", "images")
    master_file = os.path.join(os.path.dirname(
        __file__), "..", "public", "imagelist.json")
    master_list = json.load(open(master_file))
    for info in master_list:
        if 'local_url' not in info:
            info['local_url'] = os.path.join(
                "images", os.path.basename(info['url']))
            log.info("add local_url to info")
        get_img(info['url'], output_dir)
    json.dump(master_list, open(master_file, 'w'), indent=2)


def get_img(url, output_dir):
    """ download the image to your local disk"""
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    image_file = os.path.basename(url)
    image_file = os.path.join(output_dir, image_file)
    if not os.path.isfile(image_file):
        log.info("download image file: %s", image_file)
        urllib.request.urlretrieve(url, image_file)
    else:
        log.info("image file %s already exists", image_file)


if __name__ == "__main__":
    update_image_list()
