# APOD - Astronomy Picture Of the Day

This is a project to build a digital picture frame that displays the last month of NASA's astronomy picture of the day photos.
The slideshow is served out by nginx on a raspberry pi. On boot the pi starts chrome in full screen mode and loads the pictures.html page.
The page reads an image list that it uses to cycle the pictures. The image list is updated periodically throughout the day since the 
raspberry pi might not be turned on at all times.

NASA image URL: https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY&count=10

The webpage for the digital picture frame is served out by gitlab pages at https://drkgbauer.gitlab.io/apod/


## TODO
1. Install scripts
2. Make it work with local files or with remote
3. Extend apod.py to have command line args to support install, local images, and remote
4. Rewrite README to not have Python3 install but use the one that comes with Raspbian.
5. Add copyright info when present.

## Parts

1. Raspberry Pi (with power supply and SD card)
2. A monitor
3. mounting hardware (I used a Zebra case with a VESA mounting plate, so that I could mount the Pi on the back of the monitor)
4. HDMI cable

## Setup

I started with Raspbian installed on my Raspberry Pi. Here is what I did to configure APOD.

Once we have python installed, we will update pip and install pyyaml (used for logging configuration)

```sh
sudo apt update
sudo apt upgrade

sudo pip3 install --upgrade pip
sudo pip3 install pyyaml

sudo mkdir -p ~/projects
cd ~/projects
sudo git clone git@gitlab.com:drkgbauer/apod.git
sudo mkdir -p /data/logs
sudo chown pi /data/logs
sudo chmod -R 777 /data
```

We will run `apod.py` to make sure everything is working

```sh
python3 ~/projects/apod/apod.py
```


### Install nginx

```sh
sudo apt-get install nginx
/etc/init.d/nginx start
cp ~/projects/apod/public/* /var/www/html/
cd /var/www
sudo chown -r pi:pi /var/www
```


### Create the crontab entry
This goes out and updates the list of images that are being displayed in the 
slideshow. NASA's APOD site only puts up a new image once per day, but I run 
this script every 10 minutes since I don't know when the Pi will be powered on.
The update script also randomizes the order of the pictures.

```sh
crontab -e
```

add the following line to your crontab

```
0 * * * * ~/apod/update_images.sh > /data/image_update.out 2>&1
```

or just apply the crontab file from this project

```
crontab crontab.apod
```

The pictures.html page uses javascript to read the image list and then scroll
through the images. It updates the image list every 10 minutes to pick up any 
changes (new picture once per day and new random order).

### Update the image list and copy the static files into the html director for ngingx

I ran the update script just to get things started. You could wait for the 5 
minute boundary and the cron job would do this (I'm inpatiant)

```sh
cd ~/apod/apod
./update_images.sh
```

### Run Chromium in kiosk mode

I got the kiosk script (`kiosk.sh`) from [pymylifeup](https://pimylifeup.com/raspberry-pi-kiosk/). The main thing 
is to get the script to clean up when the browser doesn't exit cleanly. The kiosk.sh script has 
a few lines of `sed` that clean up the chromium prefrences. 

Additionally, the `kiosk.service` script can be used to run the `kiosk.sh` script as startup. I didn't get chromium
launch this way, so I'll look for another way to launch chromium at boot.

### Pages link

The pages link is under setting->pages: [apod pages link](https://drkgbauer.gitlab.io/apod).
