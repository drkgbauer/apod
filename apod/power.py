import argparse
import RPi.GPIO as gpio

PWR_PIN=17

gpio.setmode(gpio.BCM)
gpio.setup(PWR_PIN, gpio.OUT)
gpio.PWM(PWR_PIN, 50)

def power_on():
    """ Set the GPIO voltage to high to turn on the power on the relay"""
    gpio.output(PWR_PIN, gpio.HIGH)
    
def power_off():
    """ Set the GPIO voltage to low to turn off the power on the relay"""
    gpio.output(PWR_PIN, gpio.LOW)
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("power", help="power state to put the device into 'on', 'off'")
    args = parser.parse_args()
    if args.power.lower() == 'off':
        power_off()
    else:
        power_on()
        
        