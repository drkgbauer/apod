"""
Helper functions to setup python logging
"""
import os
import time
import math
import logging
import logging.config
from logging import handlers
import yaml

#getLogger = logging.getLogger
LOGGER = logging.getLogger(__name__)


def setup_logging(name, cfg='logging.yml', subdir=True):
    """
    Setup logging configuration
    """
    root = os.path.splitext(os.path.basename(name))[0]
    print(cfg)
    if os.path.exists(cfg):
        with open(cfg, 'rt') as fd:
            config = yaml.safe_load(fd.read())
        fhandler = config['handlers']['file_handler']
        if subdir:
            filepath = os.path.join(
                os.path.dirname(fhandler['filename']), root)
        else:
            filepath = os.path.join(os.path.dirname(fhandler['filename']))
        if not os.path.isdir(filepath):
            os.makedirs(filepath)
        fhandler['filename'] = os.path.join(filepath, root+".log")
        logging.config.dictConfig(config)
    else:
        print("using basicConfig")
        logging.basicConfig(level='DEBUG')
    return logging.getLogger(name)


def library_logger(name):
    """logger with a null handler"""
    log = logging.getLogger(name)
    if log.handlers:
        log.addHandler(logging.NullHandler())
    return log


class MyTimedRotatingFileHandler(handlers.TimedRotatingFileHandler):
    """
    Handler for logging to a file, rotating the log file at certain timed
    intervals.

    If backupCount is > 0, when rollover is done, no more than backupCount
    files are kept - the oldest ones are deleted.
    """

    def __init__(self, filename, when='h', interval=1, backupCount=0, encoding=None,
                 delay=False, utc=False, compact_datefmt=True):
        handlers.TimedRotatingFileHandler.__init__(self, filename, when, interval,
                                                   backupCount, encoding, delay, utc)
        # make sure that we start at the beginning of the interval
        self.rolloverAt = math.floor(
            self.rolloverAt/self.interval)*self.interval
        current_time = int(time.time())
        LOGGER.info("rolloverAt: %s", time.strftime(
            self.suffix, time.localtime(self.rolloverAt)))
        while self.rolloverAt <= current_time:
            self.rolloverAt = self.rolloverAt + self.interval
        LOGGER.info("rolloverAt: %s", time.strftime(
            self.suffix, time.localtime(self.rolloverAt)))
        if compact_datefmt:
            self.__setCompactDateFormat()
        self._setBaseFileName()
        if self.stream:
            self.stream.close()
            self.stream = None
        LOGGER.info("self.baseFilename: %s", self.baseFilename)
        if not self.delay:
            self.stream = self._open()

    def __setCompactDateFormat(self):
        # Calculate the real rollover interval, which is just the number of
        # seconds between rollovers.  Also set the filename suffix used when
        # a rollover occurs.  Current 'when' events supported:
        # S - Seconds
        # M - Minutes
        # H - Hours
        # D - Days
        # midnight - roll over at midnight
        # W{0-6} - roll over on a certain day; 0 - Monday
        #
        # Case of the 'when' specifier is not important; lower or upper case
        # will work.
        import re
        if self.when == 'S':
            self.suffix = "%Y%m%d_%H%M%S"
        elif self.when == 'M':
            self.suffix = "%Y%m%d_%H%M"
        elif self.when == 'H':
            self.suffix = "%Y%m%d_%H"
        elif self.when == 'D' or self.when == 'MIDNIGHT':
            self.suffix = "%Y%m%d"
        elif self.when.startswith('W'):
            self.suffix = "%Y%m%d"
        self.extMatch = re.compile(self.extMatch)

    def _setBaseFileName(self):
        """
        Strip off the old suffix and put on a new suffix
        """
        basefilename, ext = os.path.splitext(self.baseFilename)
        # If this is the first time through we haven't tacked on the date.
        if ext == ".log":
            # TODO: We really should do a regular expression here to handle things other
            # than .log
            basefilename = self.baseFilename
        ftime = self.rolloverAt-self.interval
        if self.utc:
            time_tuple = time.gmtime(ftime)
        else:
            time_tuple = time.localtime(ftime)
        self.baseFilename = basefilename+"." + \
            time.strftime(self.suffix, time_tuple)
        LOGGER.info(self.baseFilename)

    def getFilesToDelete(self):
        """
        Determine the files to delete when rolling over.
        """
        dir_name, base_name = os.path.split(self.baseFilename)
        base_name, suffix = os.path.splitext(base_name)
        result = [x for x in os.listdir(dir_name) if x.startswith(base_name) and
                  x != self.baseFilename]
        result.sort()
        if len(result) < self.backupCount:
            result = []
        else:
            result = result[:len(result) - self.backupCount]
        return result

    def doRollover(self):
        """
        do a rollover; in this case, we just need to set the new filename and delete old files.
        """
        if self.stream:
            self.stream.close()
            self.stream = None

        current_time = int(time.time())
        dst_now = time.localtime(current_time)[-1]

        if self.backupCount > 0:
            for ssss in self.getFilesToDelete():
                os.remove(ssss)
        if not self.delay:
            self.stream = self._open()
        new_rollover_at = self.computeRollover(current_time)
        while new_rollover_at <= current_time:
            new_rollover_at = new_rollover_at + self.interval
        # If DST changes and midnight or weekly rollover, adjust for this.
        if (self.when == 'MIDNIGHT' or self.when.startswith('W')) and not self.utc:
            dst_at_rollover = time.localtime(new_rollover_at)[-1]
            if dst_now != dst_at_rollover:
                if not dst_now:  # DST kicks in before next rollover, so we need to deduct an hour
                    addend = -3600
                else:           # DST bows out before next rollover, so we need to add an hour
                    addend = 3600
                new_rollover_at += addend
        self.rolloverAt = new_rollover_at
        self._setBaseFileName()
        LOGGER.info("self.baseFilename: %s", self.baseFilename)
        LOGGER.info("rolloverAt: %s", time.strftime(
            self.suffix, time.localtime(self.rolloverAt)))
