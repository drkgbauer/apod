#!/bin/bash
xset s noblank
xset s on
xset s 300
xset -dpms

unclutter -idle 0.5 -root &

sed -i 's/"exited_cleanly":false/"exited_cleanly":true/' /home/pi/.config/chromium/Default/Preferences
sed -i 's/"exit_type":"Crashed"/"exit_type":"Normal"/' /home/pi/.config/chromium/Default/Preferences

/usr/bin/chromium-browser --noerrdialogs --disable-infobars --kiosk http://localhost &
#/usr/bin/chromium-browser --noerrdialogs --disable-infobars  http://localhost &

# while true; do
#    xdotool keydown ctrl+Tab; xdotool keyup ctrl+Tab;
#    sleep 10
# done